package com.techu.apitechu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "¡¡Hola mundo ApiTechU v1!!!";
    }
    @RequestMapping("hello")
    //Al poner RequestParam quiere decir que eel parámetro va a venir de "fuera" y podemos poner un valor por defecto
    public String hello(@RequestParam (value="name", defaultValue = "Tech U") String name){
       //sustituye y lo formatea, diferente de poner sólo "+" que sólo concatena
        return String.format ("Hola %s!", name);
    }

}
