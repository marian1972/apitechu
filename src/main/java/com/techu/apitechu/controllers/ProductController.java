package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping (APIBaseUrl+"/products")
    public ArrayList<ProductModel> getProduct (){
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping (APIBaseUrl+"/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es:" + id);
        ProductModel result = new ProductModel();
        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result = product;
            }
        }

        return result;
    }
    
    @PostMapping (APIBaseUrl+"/products")
    public ProductModel createProduct (@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println(("La id del nuevo producto es: " + newProduct.getId()));
        System.out.println(("La descripción del nuevo producto es: " + newProduct.getDesc()));
        System.out.println(("El precio del nuevo producto es: " + newProduct.getPrice()));

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping (APIBaseUrl+"/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("id recibido por parámetro : " + id);
        System.out.println("El id del producto actualizado: " + product.getId());
        System.out.println(" La descripción del producto es: "+ product.getDesc());
        System.out.println("El precio del producto es: " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels)
        {
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @DeleteMapping (APIBaseUrl+"/products/{id}")
    public ProductModel deleteProduct (@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("id recibido por parámetro, producto a borrar : " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;


        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundProduct = true;
                result = product;
            }
            if (foundProduct == true){
                ApitechuApplication.productModels.remove(product);
                System.out.println("Se borra el producto");
            }

        }

        return result;
    }

    @PatchMapping (APIBaseUrl+"/products/{id}")
    public ProductModel patchProduct (@RequestBody ProductModel productPatch, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("id recibido por parámetro del producto a actualizar : " + id);
        System.out.println("La descripción del producto es: " + productPatch.getDesc());
        System.out.println("El precio del producto es: " + productPatch.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id))
            {

                System.out.println("Producto encontrado");
                result = productInList;

                if (productPatch.getDesc() != null) {
                    System.out.println("Actualizo descripción" + productPatch.getDesc());
                    productInList.setDesc(productPatch.getDesc());
                }

                if (productPatch.getPrice() > 0) {
                    System.out.println("Actualizo el precio: " + productPatch.getPrice());
                    productInList.setPrice(productPatch.getPrice());
                }
            }
        }
        return result;
    }
}